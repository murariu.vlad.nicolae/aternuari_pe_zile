import requests
from datetime import date, timedelta
import re


def get_last_days_att(MACuri, number_of_days):
    rezultat = ""
    lista_MACuri = genereaza_lista_MACuri(MACuri)
    for MAC in lista_MACuri:
        if MAC_is_valid(MAC):
            rezultat += get_att_for_MAC(MAC,number_of_days)
        else:
            rezultat += f"\n\tMACul {MAC} nu este valid!\n"
    return rezultat


def get_att_for_MAC(MAC, number_of_days):
    rezultat = f"\n\t{MAC}\n"
    MAC_to_search = get_MAC_to_search(MAC)
    last_days = get_last_days(number_of_days)
    for day in last_days:
        continut_raport = get_continut_raport(day)
        try:
            state = get_state(MAC_to_search, continut_raport)
            rezultat += f"{day}: {state}\n"
        except:
            rezultat += f"{day}: NU AM GASIT!\n"  
    return rezultat


def genereaza_lista_MACuri(MACuri):
    MACuri = MACuri.strip()
    lista_MACuri = MACuri.split("\n")
    MACuri_fara_spatii = []
    for MAC in lista_MACuri:
        MACuri_fara_spatii.append(MAC.strip())
    return MACuri_fara_spatii


def MAC_is_valid(MAC):
    format_MAC = re.compile("^[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}:[a-zA-Z0-9]{2}$")
    return (format_MAC.match(MAC) is not None)


def get_MAC_to_search(MAC):
    return re.search(".{2}:.{2}:(.*)", MAC, flags = re.DOTALL).group(1).lower()


def get_last_days(number_of_days):
    last_days = []
    for days in range(number_of_days):
        last_days.append(formatare_data_raport(date.today() - timedelta(days)))
    return last_days


def formatare_data_raport(data_raport):
    return data_raport.strftime("%d.%m.%Y")


def get_continut_raport(data_raport):
    sesiune = requests.Session()
        
    url_wiki = "http://10.1.1.11/mediawiki/api.php"
    template_titlu = "Raport_pentru_atenuari_"
  
    parametri_request_content = {
    	"action": "parse",
    	"format": "json",
    	"page": f"{template_titlu}{data_raport}",
    	"formatversion": "2",
    }
     
    request_continut = sesiune.get(url=url_wiki, params=parametri_request_content)
    raspuns_request_continut = request_continut.json()
    
    return raspuns_request_continut["parse"]["text"].lower()


def get_state(MAC, raport):
    table_row = re.search(f"(<tr>.*{MAC}.*?</tr>)", raport, flags = re.DOTALL).group(1)
    att = get_att(MAC, table_row)
    status = get_status(MAC, table_row)
    if int(status) != 1:
        return f"{att} status {status}"
    else:
        return att


def get_att(MAC, raport):
    rand_att = re.search(f"{MAC}.*?\n.*?\n(.*?)\n", raport, flags = re.DOTALL).group(1)
    att = re.search("(-\d{2}\.\d{,2}|0)", rand_att, flags = re.DOTALL).group(1)
    return att


def get_status(MAC, raport):
    status = re.search(f"\n.*(\d).*\n.*{MAC}", raport, flags = re.DOTALL).group(1)
    return status

