from tkinter import *
from tkinter import scrolledtext
from get_att import *

window = Tk()
window.title("Atenuari pe zile")
window.geometry('1100x800')

label1 = Label(window, text="Returneaza atenuarile pentru ultimele ")
label1.grid(column = 0, row = 0)

numar_zile = Entry(window, width = 10)
numar_zile.grid(column = 1, row = 0)

label2 = Label(window, text="zile pentru urmatoarele MAC ONU:\n(Daca introduceti mai multe MAC ONU separati-le cu un enter!) ")
label2.grid(column = 2, row = 0)

MACuri_container = scrolledtext.ScrolledText(window, width = 30, height = 10)
MACuri_container.grid(column = 3, row = 0)

def afiseaza_atenuarile():
    separator = "\n=================================================="
    try:
        number_of_days = int(numar_zile.get().strip())
        MACuri = MACuri_container.get("1.0", "end")
        output = f"{get_last_days_att(MACuri, number_of_days)}{separator}"
        rezultat.insert(INSERT, output)
    except:
        rezultat.insert(INSERT, f"\n\tNUMARUL DE ZILE NU ESTE VALID!{separator}")

afiseaza_atenuarile = Button(window, text = "Afiseaza atenuarile", command = afiseaza_atenuarile)
afiseaza_atenuarile.grid(column = 4, row = 0)


label3 = Label(window, text="Rezultatul va fi afisat jos!")
label3.grid(column = 0, row = 1, columnspan = 4)

rezultat = scrolledtext.ScrolledText(window, width = 100, height = 30)
rezultat.grid(column = 0, row = 2, columnspan = 4)

window.mainloop()

